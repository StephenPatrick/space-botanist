Made in six days for the relaxation jam in July 2015 at Game Jolt.

Space Botanist is a chill sandbox game where you, the space botanist, plant and cross-breed plants from earth and from Pluto. There's no time limit, no points; there's just you, your spaceship and your plants.

Controls: WASD to move, arrow keys and numbers to select inventory items, space to interact with the environment. Press M to toggle music.

The environment:
Soil: Use the hoe on soil to prepare it to plant seeds. Water plants every day to let them grow and cross-breed. Hoe the land between two plants and water both, flowered plants before going to sleep to try to cross-breed the two plants.

There are fifteen different plants to discover!

The shovel will let you replace one kind of soil with another, and will pick up whatever plant was in that spot. Plants bred from Plutonian flowers can only be grown on Plutonian soil, and the same goes for Earth flowers.

Oven: You can cook the flowers that you grow to see how they taste!

Bed: Interact with the bed to sleep until the next day.

Colorful Buttons: Interact with these to change the lighting in your spaceship.

Levers: Speed up or slow down your ship. One lever controls your ship's horizontal movement, and the other vertical movement. Hold SHIFT when interacting to reverse speed. Hold CTRL when interacting to return to default speed.

Space Botanist uses Crafty.js as it's engine, and runs best on Chrome.