Game = {

	// The smallest width a tile can have.
	tileWidth: function() {
		return 32;	
	},
	
	// The smallest height a tile can have.
	tileHeight: function() {
		return 16;	
	},
	
	// Make a diamond from a rectangle appropriate
	// for a single tile's shape. 
	makeDiamond: function(width, height, xOffset, yOffset) {	
		return new Crafty.polygon([xOffset+width/2,yOffset+height],
								  [xOffset+width,yOffset+height/2],
								  [xOffset+width/2,yOffset],
								  [xOffset,yOffset+height/2]);
	},
	
	// The total width of the game screen. 
	width: function() {
		return Crafty.DOM.window.width;
	},
 
	// The total height of the game screen. 
	height: function() {
		return Crafty.DOM.window.height;
	},
	
	grid: Crafty.isometric.size(32,16),
	
	EarthUsables: function () {
		return ["Watering Can", "Hoe", "Shovel", "Earth Soil", "Plutonian Soil", 
				"Potato", "Sunflower", "Moonflower", "Whiteflower", "Coupleflower",
				"Bulbflower", "Pinkflower"];
	},
	
	itemTexts: { "Watering Can":  "The oven boiled the water!",
			     "Hoe":           "If I need iron . . .",
			     "Shovel":        "The hoe was bad enough . . .",
			     "Earth Soil":    "Tastes like home.",
			     "Plutonian Soil":"Not enough carbon.",
			     "Potato":        "Delicious!",
			     "Sunflower":     "Could use some salt.",
			     "Moonflower":    "Tastes like cheese?",
			     "Plutoflower":   "Like eating a lightning bolt.",
			     "Charonflower":  "Like frog eyes . . .",
			     "Whiteflower":   "Wonderful!",
			     "Coupleflower":  "It makes me tear up . . .",
			     "Bulbflower":    "Smoky! Gives me the hiccups!",
			     "Eggflower":     "Who knew chickens came from Pluto?",
			     "Plagueflower":  "I vomited.",
			     "Blacksunflower":"I'm not sure that was edible . . .",
			     "Dwarfsunflower":"Feeling a little tipsy?",
			     "Pennyflower":   "Like taking bites out of my wallet.",
			     "Nixflower":     "Like burned black coffee.",
			     "Pinkflower":    "Still tastes like potato."},
	
	AlienUsables: function () {
		return ["Watering Can", "Hoe", "Shovel", "Earth Soil", "Plutonian Soil", 
				"Plutoflower", "Charonflower", "Eggflower", "Plagueflower", "Blacksunflower",
				"Dwarfsunflower", "Pennyflower", "Nixflower"];
	},
	
	crossArray: [["Potato", "Whiteflower", "Bulbflower", "Eggflower", "Plagueflower","Whiteflower"],
				 ["Whiteflower", "Sunflower", "Coupleflower", "Dwarfsunflower", "Blacksunflower","Whiteflower"],
				 ["Bulbflower", "Coupleflower", "Moonflower", "Charonflower", "Nixflower","Bulbflower"],
				 ["Eggflower", "Dwarfsunflower", "Charonflower", "Plutoflower", "Pennyflower","Charonflower"],
				 ["Plagueflower", "Blacksunflower", "Nixflower", "Pennyflower", "Charonflower","Plagueflower"],
				 ["Pinkflower","Whiteflower","Bulbflower","Charonflower","Plagueflower","Whiteflower"]
				],
	
	plantList: ["Potato","Sunflower","Moonflower", "Plutoflower", "Charonflower",
				"Whiteflower", "Coupleflower", "Bulbflower", "Eggflower", "Plagueflower",
				"Blacksunflower", "Dwarfsunflower", "Pennyflower", "Nixflower", "Pinkflower"],
	
	
	// Each corresponds to what should be instantiated
	// for integers in gridArray. 
	tiles: ["","Glass","BrownSoil","Grass","PurpleSoil", //0-4
			"PathFSlash","PathBSlash","PathU","PathCross","PathN", //5-9
		   	"Path3", "PathC","PathDot","Oven","ZebraFloor", //10-14
		   	"Sink", "WallHole", "Block", "BedBottom", "BedTop", //15-19
			"ClosetTop", "ClosetBottom", "Lever1", "Lever2", "ControlPanel"], //20-24
	
	lightRoomArray: [
				[[0,0,0,0,0],
				 [0,0,0,0,0],
				 [0,0,0,0,17],
				 [0,0,0,0,0],
				 [17,17,17,17,17],
				 [17, 17, 17, 0, 0]],
				//layer 1
				[[0,0,0,0,0],
				 [0,17,17,17,0],
				 [0,17,17,17,17],
				 [17,17,17,17,0],
				 [0,12,17,17,17],
				 [6, 17, 17, 0, 0]],
				//layer 2
				[[0,17,17,17,17],
				 [0,0,24,0,0],
				 [0,17,0,0,17],
				 [17,0,0,0,0],
				 [0,0,0,0,17],
				 [0,0,0,0,0]],
				//layer 3
				[[0,16,17,17,17],
				 [0,0,0,0,0],
				 [0,17,0,0,17],
				 [17,0,0,0,0],
				 [0,0,0,0,17],
				 [0,0,0,0,0]],
				//layer 4
				[[0,17,17,17,17],
				 [0,0,0,0,0],
				 [0,17,0,0,17],
				 [17,0,0,0,0],
				 [0,0,0,0,17],
				 [0,0,0,0,0]]],
	
	cookRoomArray: [
				[[0,0,0,0,0],
				 [0,0,0,0,0],
				 [0,0,0,0,0],
				 [0,0,0,0,0],
				 [0,17,17,0,17],
				 [0, 0, 17, 17, 17]],
				//layer 1
				[[0,17,17,17,17],
				 [0,17,17,17,17],
				 [0,17,17,17,17],
				 [0,17,17,17,17],
				 [0,17,17,17,12],
				 [0, 0, 17, 17, 5]],
				//layer 2
				[[0,17,17,17,17],
				 [0,17,17,15,17],
				 [0,17,13,0,15],
				 [0,0,0,0,17],
				 [0,17,0,0,0],
				 [0,0,0,0,0]],
				//layer 3
				[[0,17,17,17,17],
				 [0,0,0,0,17],
				 [0,17,0,0,0],
				 [0,0,0,0,17],
				 [0,17,0,0,0],
				 [0.0,0,0,0]],
				//layer 4
				[[0,17,17,17,17],
				 [0,0,0,0,17],
				 [0,17,0,0,0],
				 [0,0,0,0,17],
				 [0,17,0,0,0],
				 [0,0,0,0,0]]],
	
	controlRoomArray: [
				[[0,17,17,17,17],
				 [0,17,17,17,17],
				 [0,17,17,17,17],
				 [0,17,17,17,17],
				 [0,0,17,17,17]],
				//layer 1
				[[0,17,17,17,17],
				 [0,17,17,17,17],
				 [0,12,17,17,17],
				 [0,17,17,17,17],
				 [0,0,17,17,17]],
				//layer 2
				[[0,17,17,17,17],
				 [0,0,22,0,17],
				 [0,0,0,23,0],
				 [0,0,0,0,17],
				 [0,0,0,0,0]],
				//layer 3
				[[0,17,17,17,17],
				 [0,0,0,0,17],
				 [0,0,0,0,0],
				 [0,0,0,0,17],
				 [0,0,0,0,0]],
				//layer 4
				[[0,17,17,17,17],
				 [0,0,0,0,17],
				 [0,0,0,0,0],
				 [0,0,0,0,17],
				 [0,0,0,0,0]]],
	
	sleepRoomArray: [
				[[17,17,17,17,17],
				 [17,17,17,17,17],
				 [17,17,17,17,17],
				 [17,17,17,17,0],
				 [17,17,17,17,0]],
				//layer 1
				[[17,17,17,17,17],
				 [14,14,4,14,6],
				 [17,14,14,14,12],
				 [14,14,14,14,0],
				 [17,14,14,14,0]],
				//layer 2
				[[17,17,17,17,17],
				 [0,19,0,21,0],
				 [17,0,18,0,0],
				 [0,0,0,0,0],
				 [17,0,0,0,0]],
				//layer 3
				[[17,16,17,17,17],
				 [0,0,0,20,0],
				 [17,0,0,0,0],
				 [0,0,0,0,0],
				 [17,0,0,0,0]],
				//layer 4
				[[17,17,17,17,17],
				 [0,0,0,0,0],
				 [17,0,0,0,0],
				 [0,0,0,0,0],
				 [17,0,0,0,0]]],
	
	// A layer of the map, as an array.
	grassRoomArray: [
				[[0,1,1,1,1,1,1,1,1,1,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[1,0,0,0,0,0,0,0,0,0,0],
				[0,1,1,1,1,1,1,1,1,1,0],
				[0,1,1,1,1,1,1,1,1,1,0],],
				//layer 1
				[[0,1,1,1,1,1,1,1,1,1,0],
				[5,9,9,9,9,9,9,9,9,6,0],
				[1,8,7,7,7,7,7,7,7,8,1],
				[11,5,2,2,2,2,2,2,6,10,0],
				[1,10,5,2,2,2,2,2,6,11,1],
				[11,2,5,2,2,2,2,6,2,10,0],
				[1,10,2,5,2,2,2,6,2,11,1],
				[11,2,2,5,2,2,6,2,2,10,0],
				[1,10,2,2,5,2,6,2,2,11,1],
				[11,2,2,2,5,6,2,2,2,10,0],
				[1,10,2,2,2,8,2,2,2,11,1],
				[11,2,2,2,6,5,2,2,2,10,0],
				[1,10,2,2,6,2,5,2,2,11,1],
				[11,2,2,6,2,2,5,2,2,10,0],
				[1,10,2,6,2,2,2,5,2,11,1],
				[11,2,6,2,2,2,2,5,2,10,0],
				[1,10,6,2,2,2,2,2,5,11,1],
				[11,6,2,2,2,2,2,2,5,10,0],
				[1,8,9,9,9,9,9,9,9,8,1],
				[6,7,7,7,7,7,7,7,7,5,0], 
				[0,1,1,1,1,1,1,1,1,1,0],],
				//layer 2
				[[0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0], 
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],],
				//layer 3
				[[0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0], 
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],],
				//layer 4
				[[0,1,1,1,1,1,1,1,1,1,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0], 
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0,0,0,0],]],
	
	makeRoom: function (grid, offsetX, offsetY, offsetZ) {
		for(var z = 0; z < grid.length; z++){
			for(var y = 0; y < grid[0].length; y++){
				for(var x = 0; x < grid[0][0].length; x++){
					var tile = undefined;
					if ((typeof grid[z][y][x]) === "string"){
						tile = Crafty.e(grid[z][y][x]);
					} else if (grid[z][y][x] > 0){
						tile = Crafty.e(Game.tiles[grid[z][y][x]]);
					}
					if (tile !== undefined){
						Game.tilePlace(x,y,z,tile,offsetX,offsetY,offsetZ);
					}
				}
			}
		}	
		
	},
	
	// Returns a corrected globalZ for given coordinates.
	globalZ: function (y,h,z) {
		var newy = h + y + (8 * z) 
		return (z * 125) + ((newy / 8) * 500);
	},
	
	// Extracts above values from a tile with y, h, and z.
	tileGlobalZ: function (tile) {
		return globalZ(tile._y,tile._h,tile._z);
	},
	
	_uiGlobalZinc: -1,
	
	uiGlobalZ: function () {
		this._uiGlobalZinc++;
		return (Math.pow(2,31)) + this._uiGlobalZinc;
	},
	
	// A smarter tile place that adds corrections and gives tiles
	// more information.
	tilePlaceBasic: function (x,y,z,tile){
		this.tilePlace(x,y,z,tile,0,0,0);
	},

	tilePlace: function (x,y,z,tile,offsetX,offsetY,offsetZ){
		// Crafty's z layers don't make sense.
		// Each incremental z layer raises rendered height by 16.
		// Until we want to use half-layers, it makes a lot more sense
		// to just ignore those half layers and only use 32-height placements.
		var newz = z + z + 1;
		this.grid.place(x+offsetX,y+offsetY,newz+offsetZ,tile);
		tile.tilex = x+offsetX;
		tile.tiley = y+offsetY;
		tile._z = newz+offsetZ;
		tile._globalZ = this.globalZ(tile._y,tile._h/2,newz+offsetZ);
	},
	
	// Initialize and start
	start: function() {
		Crafty.init(Game.width(), Game.height());
		Crafty.pixelart(true);
		Crafty.background('rgb(32, 24, 40)');
		Crafty.scene('Loading');
	}
};
 
$text_css = { 'size': '24px', 'family': 'Arial', 'color': 'white', 'text-align': 'center' };
