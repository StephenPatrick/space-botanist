Crafty.scene('plant_room', function() {
	
	var space = Crafty.e("Space").attr({x: 0, y: 0});
	space._globalZ = -100000;

	Game.grid.centerAt(14,20);
	
	Game.makeRoom(Game.cookRoomArray, -4, -4, 0);
	Game.makeRoom(Game.lightRoomArray, 9, -4, 0);
	Game.makeRoom(Game.grassRoomArray, 0, 0, 0);
	Game.makeRoom(Game.sleepRoomArray, -4, 18, 0);
	Game.makeRoom(Game.controlRoomArray, 9, 18, 0);
	
	Crafty.audio.play('song',-1);	
	Crafty.audio.togglePause('song');
	
	var botanist = Crafty.e("Botanist");
	Game.tilePlaceBasic(5,5,2,botanist);
	Crafty.viewport.scale(2);
	Crafty.bind("KeyUp", function (data) {
		if (data.key == Crafty.keys.M){	
			Crafty.audio.togglePause('song');
		}
	});
});

Crafty.scene('Loading', function(){
	Crafty.load([
		'assets/tiles.png',
		'assets/botanist.png',
		'assets/item_bar.png',
		'assets/space.png',
		], function(){
		
		Crafty.sprite(32,32,'assets/tiles.png', {
			spr_soil:        		 [0, 0],
			spr_purple_soil: 		 [1, 0],
			spr_dug_soil:			 [0, 1],
			spr_dug_purple_soil:	 [1, 1],
			spr_pool:        		 [0, 9],
			spr_grass:       		 [1, 9],
			spr_treaded_soil:		 [0, 2],
			spr_treaded_purple_soil: [1, 2],
			spr_u_path:				 [0, 3],
			spr_3_path:				 [1, 3],
			spr_fslash_path:		 [0, 4],
			spr_n_path:				 [1, 4],
			spr_bslash_path:		 [0, 5],
			spr_glass:			     [1, 7],
			spr_zebra_floor:		 [0, 7],
			spr_oven:				 [1, 6],
			spr_dot_path:	  		 [0, 6],
			spr_cross_path:			 [1, 5],
			spr_closet_bottom:		 [0, 8],
			spr_closet_top:			 [1, 8],
			spr_sink:				 [2, 0],
			spr_wall_hole:			 [3, 0],
			spr_block:				 [2, 1],
			spr_bed_bottom:			 [3, 1],
			spr_bed_top:			 [2, 2],
			spr_earth_plant_small:   [3, 2],
			spr_earth_plant_med:     [2, 3],
			spr_alien_plant_small:   [3, 3],
			spr_earth_plant_large:   [2, 4],
			spr_alien_plant_med:     [3, 4],
			spr_potato:				 [2, 5],
			spr_alien_plant_large:   [3, 5],
			spr_sunflower:			 [2, 6],
			spr_plutoflower:		 [3, 6],
			spr_moonflower:			 [2, 7],
			spr_charonflower:		 [3, 7],
			spr_whiteflower:  		 [1, 13],
			spr_bulbflower:			 [1, 14],
			spr_coupleflower:		 [1, 15],
			spr_dwarfsunflower:		 [0, 14],
			spr_blacksunflower: 	 [0, 15],
			spr_eggflower:			 [2, 14],
			spr_nixflower:			 [2, 15],
			spr_pennyflower:		 [3, 15],
			spr_plagueflower:		 [3, 14],
			spr_pinkflower:			 [3, 13],
			spr_lever_down:			 [2, 8],
			spr_lever:				 [3, 8],
			spr_lever_up:			 [2, 9],
			spr_colorful_buttons:	 [3, 9]
		// Padding x, y
		}, 2, 2);
		
		Crafty.sprite(2540,1920,'assets/space.png',{
			spr_space:			 	 [0, 0]
		}, 2, 2);
		
		Crafty.sprite(80,2000,'assets/item_bar.png',{
			spr_item_bar: 			 [0, 0]
		}, 2, 2);
		
		Crafty.sprite(23,36,'assets/botanist.png',{
			spr_botanist: 			 [3, 0]
		}, 2, 2);
		
		Crafty.audio.add("song", ["assets/the_space_botanist_2.wav",
								  "assets/the_space_botanist_2.mp3",
								  "assets/the_space_botanist_2.ogg"]);
		
		Crafty.scene('plant_room');
	});
});