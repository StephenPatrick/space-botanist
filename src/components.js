// The components file is used to hold classes, basically.
// Components are classes. Basically. I don't know if there's
// function overwriting.

/**
 ** CHARACTERS 
 **
 */
		
Crafty.c('Botanist', {
	init: function () {
		this.tilex = 0;
		this.tiley = 0;
		this.target = Crafty.e('Target');
		this.high_target = Crafty.e('Target'); 
		this.requires('Actor, PlayerInput, SpriteAnimation, spr_botanist, IsoFourway, Delay')
			.collision(Game.makeDiamond(14,7,5,this._h-6))
			.reel('Left',600, 2, 0, 1)
			.reel('DownLeft',600, 3, 0, 1)
			.reel('DownLeftWalk', 600, 0, 1, 4)
			.reel('UpLeftWalk', 600, 0, 2, 4)
			.reel('LeftWalk', 600, 0, 3, 4)
			.reel('UpLeft',600, 4, 0, 1)
			//.bind('KeyUp', function (data) {
			//		
			//})
			.bind('Draw', function () {
				this.tiley = (this._y + this._h + 18);
				this.tilex = (this._x + (this._w / 2));
				this._globalZ = Game.globalZ(this._y,this._h-8,this._z);
				this.target.x = this.tilex + (this.directionx * Game.tileWidth()/1.5);
				this.target.y = this.tiley + (this.directiony * Game.tileHeight()/1.5);
				this.target.z = this._z;
				this.high_target.x = this.tilex + (this.directionx * Game.tileWidth()/1.5);
				this.high_target.y = this.tiley + (this.directiony * Game.tileHeight()/1.5) - 16;
				this.high_target.z = this._z + 2;
			})
			.bind('EnterFrame', function () {
				if (this.tileBaseCollision('SolidBase') ||
					!this.tileTopCollision('SolidTile')){
					this.stopMoving();	
				}
				//follow that incoroporates scaling
				Crafty.viewport.scroll('_x',-1*(this._x-Game.width() / 4));
				Crafty.viewport.scroll('_y',-1*(this._y-Game.height() / 4));
			})
			.bind('NewDirection', function (data) {
				if (!this.sleeping) {
					this.animateFrame(data);
				}
			})
			.bind('KeyDown', function () {
				if (this.isDown("SPACE")){
					this.use();	
				}
				if (this.isDown("UP_ARROW")){
					this.showItem(-1,-1);	
				} else if (this.isDown("DOWN_ARROW")){
					this.showItem(1,-1);
				} else if (this.isDown("1")) {
					this.showItem(0,0);	
				} else if (this.isDown("2")) {
					this.showItem(0,1);	
				} else if (this.isDown("3")) {
					this.showItem(0,2);	
				} else if (this.isDown("4")) {
					this.showItem(0,3);	
				} else if (this.isDown("5")) {
					this.showItem(0,4);	
				} else if (this.isDown("6")) {
					this.showItem(0,5);	
				} else if (this.isDown("7")) {
					this.showItem(0,6);	
				} else if (this.isDown("8")) {
					this.showItem(0,7);	
				} else if (this.isDown("9")) {
					this.showItem(0,8);	
				} else if (this.isDown("0")) {
					this.showItem(0,9);	
				}
			})
			.bind("Sleep", function () {
				this.sleep();
			})
			.bind("PickedUp", function (plantindex) {
				var added = this.itemToolTip.addItem(Game.plantList[plantindex]);
				if (added) {
					this.items.push(Game.plantList[plantindex]);	
				}
			})
			.bind("Eat", function () {
				this.say(Game.itemTexts[this.items[this.inHand]]);
			});
		this.itemToolTip = Crafty.e('itemToolTip');
		this.itemToolTip.addItems(this.items);
		this.itemToolTip.update(0);
		this.animateFrame();
	},
	
	say: function (text) {
		var delayFunction = function () {this.textObject.destroy();};
		if (this.textObject !== undefined){
			this.textObject.destroy();
		}
		this.textObject = Crafty.e("2D, Canvas, Text, UI").text(text).textColor('rgb(255,255,255)')
						   .attr({x: this.x-40, y: this.y - 12});
		this.attach(this.textObject);
		this.delay(delayFunction, 2500);
	},
	
	sleep: function (bed) {
		if (!this.sleeping){
			this.sleeping = true;
			this.disableControl();
			
			// Transition animation
			transition = Crafty.e('Actor, Tween, Color, UI').attr({x: -1 * Crafty.viewport._x,
														y: -1 * Crafty.viewport._y,
														w: Game.width(),
														h: Game.height()});
			transition.color('rgba(0,0,0,1)').attr({alpha: 0});
			transition.tween({alpha: 1}, 1500);
			transition.one('TweenEnd', function () {
				if (this.alpha === 0){
					this.color('rgb(255,255,255)');
					this.destroy();
				} else {
					Crafty.trigger('NewDay');
					this.tween({alpha: 0}, 1500);
				}
			});
			this.delay(function () {this.sleeping = false; this.enableControl()}, 1750);
		}
	},
	
	use: function () {
		if (this.target.targetedTile !== undefined && this.target.targetedTile.has("Soil")){
			this.target.targetedTile.use(this.items[this.inHand]);	
		} else if (this.high_target.targetedTile !== undefined) {
			this.high_target.targetedTile.interact(this.isDown("SHIFT"),this.isDown("CTRL"));
		}
	},
	
	animateFrame: function () {
		var data = this._movement;
		if (data.x === 0 && data.y === 0){
			this.animateIdle();
		} else if (this.isDown('SHIFT')) {
			//Strafe
		} else if (data.y == 0 && data.x < 0) {
			this.animate('LeftWalk', -1);
			this._flipX = false;
			this.directionx = -1;
			this.directiony = 0;
		} else if (data.y == 0 && data.x > 0) {
			this.animate('LeftWalk', -1);
			this._flipX = true;
			this.directionx = 1;
			this.directiony = 0;
		} else if (data.y > 0 && data.x == 0) {
			this.animate('DownLeftWalk', -1);
			this.directionx = 0;
			this.directiony = 1;
		} else if (data.y < 0 && data.x == 0) {
			this.animate('UpLeftWalk', -1);
			this.directionx = 0;
			this.directiony = -1;
		} else if (data.y < 0 && data.x < 0) {
			// Moving Left
			this.animate('UpLeftWalk', -1);
			this._flipX = false;
			this.directionx = -1;
			this.directiony = -1;
		} else if (data.y > 0 && data.x > 0) {
			// Moving Right
			this.animate('DownLeftWalk', -1);
			this._flipX = true;
			this.directionx = 1;
			this.directiony = 1;
		} else if (data.y < data.x) {
			// Moving Up
			this.animate('UpLeftWalk', -1);
			this._flipX = true;
			this.directionx = 1;
			this.directiony = -1;
		} else if (data.y > data.x) {
			// Moving Down
			this.animate('DownLeftWalk', -1);
			this._flipX = false;
			this.directionx = -1;
			this.directiony = 1;
		}
	},
	
	animateIdle: function () {
		if (this.directiony === 0) {
			this.animate('Left', -1);
			if (this.directionx === 1){
				this._flipX = false;
			} else {
				this._flipX = true;
			}
		} else if (this.directionx === 0) {
			this._flipX = false;
			if (this.directiony === -1){
				this.animate('Down', -1);
			} else {
				this.animate('Up', -1);
			}
		} else {
			if (this.directionx === -1) {
				this._flipX = false;
			} else {
				this._flipX = true;
			}
			if (this.directiony === -1){
					this.animate('UpLeft', -1);
			} else {
					this.animate('DownLeft', -1);	
			}
		}
	},
	
	inHand: 0,
	
	items: ["Watering Can","Hoe","Shovel","Earth Soil","Plutonian Soil","Potato","Sunflower","Moonflower","Plutoflower"],
	
	showItem: function (increment, assignment) {
		this.inHand += increment;
		if (assignment !== -1) {
			this.inHand = assignment;	
		}
		if (this.inHand < 0) {
			this.inHand = this.items.length - 1;
		} else if (this.inHand >= this.items.length) {
			this.inHand = 0;
		}
		this.itemToolTip.update(this.inHand);
	},

});

/**
 ** OBJECTS AND EFFECTS
 **
 */

Crafty.c('itemToolTip', {
	init: function () {
		this.requires('Actor, spr_item_bar, UI');
		this.labelYRow = 0;
		this.items = [];
		this.itemTexts = [];
		this.label = Crafty.e("Actor, Color, UI")
			.attr({x: this._x, y: this._y, w: 5, h: 11})
			.color('rgb(255,255,255)');
		this.bind('EnterFrame', function () {
			this.x = -1* Crafty.viewport._x;
			this.y = -1* Crafty.viewport._y;
			this.label.x = this._x;
			this.label.y = (this.labelYRow * 11) + this._y;
		});
		
	},
	
	update: function (labelYRow) {
		this.labelYRow = labelYRow;
	},
	
	addItems: function (items) {
		items.forEach(function(item) {
			this.addItem(item);
		}, this);
	},
	
	addItem: function (item) {
		if ($.inArray(item,this.itemTexts) === -1){
			var newItem = Crafty.e('2D, Canvas, Text, UI');
			this.attach(newItem);
			this.itemTexts.push(item);
			newItem.listLoc = this.items.length;
			newItem.bind('EnterFrame', function () {	
				newItem.x = this._parent._x + 8;
				newItem.y = this._parent._y + (this.listLoc * 11);
				newItem.text(this._parent.itemTexts[this.listLoc]);
				newItem.textColor('#6060FF');
				newItem.textFont({ type: 'italic', family: 'Arial' });
			});
			this.items.push(newItem);
			return true;
		}
		return false;
	}
});

Crafty.c('UI', {
	init: function () {
		this._globalZ = Game.uiGlobalZ();	
	},
});


Crafty.c('Target', {
	init: function () {
		this.targetedTile = undefined;
		this.requires('Actor');	
		this.bind('EnterFrame', function () {
			tile = this.tileTopCollision("SolidTile");
			if (tile && !tile.has("Flower")) {
				if (this.targetedTile !== undefined){
					this.targetedTile.dehighlight();	
				}
				this.targetedTile = tile;
				this.targetedTile.highlight();	
			} else {
				if (this.targetedTile !== undefined){
					this.targetedTile.dehighlight();
					this.targetedTile = undefined;
				}
			}
		})
	}
});

Crafty.c('Space', {
	init: function () {
		this.xspeed = -.2;
		this.yspeed = .3;
		this.requires('Actor, spr_space')
			.bind('EnterFrame', function() {
				this.x -= this.xspeed;
				this.y -= this.yspeed;
				if (this.x < -2500) {
					this.x = Game.width();	
				} else if (this.x > Game.width()) {
					this.x = -2500;
				}
				if (this.y < -2000) {
					this.y = Game.height();	
				} else if (this.y > Game.height()) {
					this.y = -2000;	
				}
			})
			.bind('XSpaceSpeedUp', function () {
				this.xspeed -= .2;
			})
			.bind('XSpaceSpeedDown', function () {
				this.xspeed += .2;
			})
			.bind('XSpaceSpeedDefault', function () {
				this.xspeed = -.2;
			})
			.bind('YSpaceSpeedUp', function () {
				this.yspeed += .2;
			})
			.bind('YSpaceSpeedDown', function () {
				this.yspeed -= .2;
			})
			.bind('YSpaceSpeedDefault', function () {
				this.yspeed = .3;
			});
	}	
});