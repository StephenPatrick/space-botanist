// Utility Components are defined as components which do not expect
// to be instantiated themselves, only used by other components. They
// provide general rules and allow for compressing requirements lists.

// An "Actor" is an entity that is drawn in 2D on canvas. 
Crafty.c('Actor', {
	init: function() {
		// Both 2D and Canvas are huge requirements that give
		// you a lot of stuff. Basically everything in a game
		// is going to use them.
		this.requires('2D, Canvas, Collision');
	},
	
	tileCollision: function (component, offsetZ){
		var hits = this.hit(component);
		var toReturn = false;
		if (hits === false) {
			return false;
		}
		hits.some(function(data) {
			var tile = data.obj;
			if (tile._z === this._z + offsetZ){
				toReturn = tile;
				return true;	
			}
		}, this);
		return toReturn;	
	},
	
	tileTopCollision: function (component) {
		return this.tileCollision(component, -2);
	},
	
	tileBaseCollision: function (component) {
		var bases = this.hit('Base');
		var toReturn = false;
		if (bases === false) {
			return false;	
		}
		bases.some(function(data) {
			var base = data.obj;
			if (base.has(component) && base._z === this._z){
				toReturn = base;
				return true;	
			}
		}, this);
		return toReturn;
	},
	
	stopMoving: function(){
		this.x -= this._movement.x;
		this.y -= this._movement.y;
	},
	
	// I wanted to call this hyper destroy.
	// What it does is it kills everything something owns and then kills it.
	// Right now that just means it destroys the base before destroying the main tile.
	isoDestroy: function(){
		if (this.base !== undefined){
			this.base.destroy();	
		}
		this.destroy();
	}
});

// Multiway casting to apply WASD to isometric directions
// instead of up/down/left/right screenwise. 
// TODO: Currently if this also bound the arrow keys to multiway
// the player could hold W and UP to go twice as fast. Figure out
// how to bind multiple keys to the same movement without this happening.
Crafty.c('IsoFourway', {
	init: function() {
		this.requires('Multiway')	
			.multiway({x:2, y:1}, {W: -45, A: -135, S: 135, D: 45})
			.enableControl();
	},
});

// Compressing input requirements
// Gamepad is nothing as of build 12
Crafty.c('PlayerInput', {
	init: function() {
		this.requires('Keyboard, Mouse, Gamepad');
	},
});

Crafty.c('Tile', {
	init: function() {
		this.requires('Actor, Color')
			.color("rgba(255,255,255,0)")
			.collision(
				Game.makeDiamond(Game.tileWidth(),Game.tileHeight(),0,0)
			)
			.bind('Draw', function() {
				this.base.attr({x: this._x, y: this._y});
				this.base._z = this._z;
				this.base.parent = this;
			});
		this.base = Crafty.e('2D, Collision, Base')
						  .collision( 			
								Game.makeDiamond(Game.tileWidth(),Game.tileHeight(),0,Game.tileHeight())
						  );
		this.attach(this.base);
	},
	
	use: function(item) {
		if (this.usable !== undefined && $.inArray(item, this.usable) !== -1){
			this.apply(item);
		}
	},
});

Crafty.c('SolidTile', {
	init: function() {
		this.requires('Tile, Solid');
		this.base.addComponent("SolidBase");
		this.highlighted = false;
	},
	
	highlight: function () {
		this.sprite(this.sheetx+4,this.sheety);
		this.highlighted = true;
	},
	
	dehighlight: function () {
		this.sprite(this.sheetx, this.sheety);
		this.highlighted = false;
	}
});
