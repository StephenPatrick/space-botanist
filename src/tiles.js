Crafty.c('Flower', {
	init: function () {
		this.requires('SolidTile, FlowerBase');
	}	
});


Crafty.c('EarthFlower', {
	init: function () {
		this.sheetx = 3;
		this.sheety = 2;
		this.requires('Actor, spr_earth_plant_small, Flower');
		this.bind('NewDay', function () {
			this.hasCrossbred = false;
			if (this.sheety === 2 && this.highlighted){
				this.sheetx = 2;
				this.sheety = 3;
			} else if (this.sheety === 3 && this.highlighted) {
				this.sheetx = 2;
				this.sheety = 4;
			} else if (this.sheety === 4 && this.highlighted) {
				this.sheetx = this.plantx;
				this.sheety = this.planty;	
			} else if (this.sheety === this.planty && this.highlighted){
				this.sheetx = this.plantx;
				this.sheety = this.planty;
				var seeker = Crafty.e("Actor").attr({w:4, h:8});
				for (var x = -1; x <= 1; x +=2){
					for (var y = -1; y <= 1; y+=2){
						seeker.x = (this._x + (this._w / 2)) + (x * Game.tileWidth());
						seeker.y = (this._y + (this._h * 1 / 4)) + (y * Game.tileHeight());
						seeker.z = this._z;
						var flower = seeker.tileCollision('Flower', 0);
						if (flower !== false && !flower.hasCrossbred) {
							seeker.x = (this._x + (this._w / 2)) + (x * Game.tileWidth() / 2);
							seeker.y = (this._y + (this._h * 3 / 4)) + (y * Game.tileHeight() / 2);
							var soilToPlant = seeker.tileTopCollision("Soil");
							console.log(soilToPlant);
							if (soilToPlant !== false){
								console.log("Placing " + Game.crossArray[this.plantindex][flower.plantindex]);
								soilToPlant.use(Game.crossArray[this.plantindex][flower.plantindex]);
							}
						}
					}
				}
				this.hasCrossbred = true;
			}
			this.sprite(this.sheetx, this.sheety);
			this.highlighted = false;
		});
	}
});
		
Crafty.c('AlienFlower', {
	init: function () {
		this.sheetx = 3;
		this.sheety = 3;
		this.requires('Actor, spr_alien_plant_small, Flower');
		this.bind('NewDay', function () {
			console.log(this.x, this.y);
			this.hasCrossbred = false;
			// Grow up as watered (highlighted)
			if (this.sheety === 3 && this.highlighted){
				this.sheetx = 3;
				this.sheety = 4;
			} else if (this.sheety === 4 && this.highlighted) {
				this.sheetx = 3;
				this.sheety = 5;
			} else if (this.sheety === 5 && this.highlighted) {
				this.sheetx = this.plantx;
				this.sheety = this.planty;
			// Crossbreed if fully grown
			} else if (this.sheety === this.planty && this.highlighted){
				this.sheetx = this.plantx;
				this.sheety = this.planty;
				var seeker = Crafty.e("Actor").attr({w:4, h:8});
				for (var x = -1; x <= 1; x +=2){
					for (var y = -1; y <= 1; y+=2){
						seeker.x = (this._x + (this._w / 2)) + (x * Game.tileWidth());
						seeker.y = (this._y + (this._h * 1 / 4)) + (y * Game.tileHeight());
						seeker.z = this._z;
						var flower = seeker.tileCollision('Flower', 0);
						if (flower !== false && !flower.hasCrossbred) {
							seeker.x = (this._x + (this._w / 2)) + (x * Game.tileWidth() / 2);
							seeker.y = (this._y + (this._h * 3 / 4)) + (y * Game.tileHeight() / 2);
							var soilToPlant = seeker.tileTopCollision("Soil");
							if (soilToPlant !== false && this.plantindex !== undefined && flower.plantindex < 6){
								console.log("Placing " + Game.crossArray[this.plantindex][flower.plantindex]);
								soilToPlant.use(Game.crossArray[this.plantindex][flower.plantindex]);
							}
						}
					}
				}
				this.hasCrossbred = true;
			}
			this.sprite(this.sheetx, this.sheety);
			this.highlighted = false;
		});
	}
});		

Crafty.c('Potato', {
	init: function () {
		this.plantx = 2;
		this.planty = 5;
		this.plantindex = 0;
		this.requires('EarthFlower');
	}
});
		
Crafty.c('Sunflower', {
	init: function () {
		this.plantx = 2;
		this.planty = 6;
		this.plantindex = 1;
		this.requires('EarthFlower');
	}
});
		
Crafty.c('Moonflower', {
	init: function () {
		this.plantx = 2;
		this.planty = 7;
		this.plantindex = 2;
		this.requires('EarthFlower');
	}
});
		
Crafty.c('Plutoflower', {
	init: function () {
		this.plantx = 3;
		this.planty = 6;
		this.plantindex = 3;
		this.requires('AlienFlower');
	}
});
	
Crafty.c('Charonflower', {
	init: function () {
		this.plantx = 3;
		this.planty = 7;
		this.plantindex = 4;
		this.requires('AlienFlower');
	}
});

Crafty.c('Whiteflower', {
	init: function () {
		this.plantx = 1;
		this.planty = 13;
		this.plantindex = 5;
		this.requires('EarthFlower');
	}
});
		
Crafty.c('Bulbflower', {
	init: function () {
		this.plantx = 1;
		this.planty = 14;
		this.plantindex = 7;
		this.requires('EarthFlower');
	}
});
		
Crafty.c('Coupleflower', {
	init: function () {
		this.plantx = 1;
		this.planty = 15;
		this.plantindex = 6;
		this.requires('EarthFlower');
	}
});
		
Crafty.c('Dwarfsunflower', {
	init: function () {
		this.plantx = 0;
		this.planty = 14;
		this.plantindex = 11;
		this.requires('AlienFlower');
	}
});
	
Crafty.c('Blacksunflower', {
	init: function () {
		this.plantx = 0;
		this.planty = 15;
		this.plantindex = 10;
		this.requires('AlienFlower');
	}
});

Crafty.c('Eggflower', {
	init: function () {
		this.plantx = 2;
		this.planty = 14;
		this.plantindex = 8;
		this.requires('AlienFlower');
	}
});
		
Crafty.c('Nixflower', {
	init: function () {
		this.plantx = 2;
		this.planty = 15;
		this.plantindex = 13;
		this.requires('AlienFlower');
	}
});
		
Crafty.c('Pennyflower', {
	init: function () {
		this.plantx = 3;
		this.planty = 15;
		this.plantindex = 12;
		this.requires('AlienFlower');
	}
});
	
Crafty.c('Plagueflower', {
	init: function () {
		this.plantx = 3;
		this.planty = 14;
		this.plantindex = 9;
		this.requires('AlienFlower');
	}
});

Crafty.c('Pinkflower', {
	init: function () {
		this.plantx = 3;
		this.planty = 13;
		this.plantindex = 14;
		this.requires('EarthFlower');
	}
});

/**
 ** TILES
 **
 */

Crafty.c('Grass', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 9;
		this.requires('SolidTile, spr_grass');
	}
});

Crafty.c('Soil', {
	apply: function (item) {
		if (item === 'Hoe' && this.sheety === 0){
			this.sheety += 2;
			this.sprite(this.sheetx, this.sheety);
		} else if (item === 'Shovel') {
			this.sheety = 1;
			this.sprite(this.sheetx, this.sheety);
			if (this.plant !== undefined) {
				Crafty.trigger("PickedUp", this.plant.plantindex);
				this.plant.destroy();
				this.plant = undefined;
			}
		} else if (item === 'Earth Soil') {
			if (this.sheety === 1){
				this.usable = Game.EarthUsables();
				this.sheetx = 0;
				this.sheety = 0;
				this.sprite(this.sheetx, this.sheety);
			}
		} else if (item === 'Plutonian Soil') {
			if (this.sheety === 1){
				this.usable = Game.AlienUsables();
				this.sheetx = 1;
				this.sheety = 0;
				this.sprite(this.sheetx, this.sheety);
			}
		} else if (item === 'Watering Can') {
			if (this.plant !== undefined) {
				this.plant.highlight();	
			}
		} else if (item !== "Hoe" && this.plant === undefined && this.sheety === 2) {
			console.log("Placing " + item);
			this.placeSeed(item);	
		}
	},
	placeSeed: function (seed) {
		newPlant = Crafty.e(seed);
		Game.tilePlaceBasic(this.tilex,this.tiley,this._z+1,newPlant);
		newPlant.y = this._y - Game.tileHeight();
		newPlant.x = this._x;
		newPlant.z = this._z + 2;
		newPlant._globalZ = Game.globalZ(this._y,Game.tileHeight()-8,newPlant._z)
		//this.attach(newPlant);
		this.plant = newPlant;
	},
});

Crafty.c('BrownSoil', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 0;
		this.requires('SolidTile, spr_soil, Soil');
		this.usable = Game.EarthUsables();
	},
});

Crafty.c('PurpleSoil', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 0;
		this.requires('SolidTile, spr_purple_soil, Soil');
		this.usable = Game.AlienUsables();
	}
});

Crafty.c('Path', {
	init: function () {
		this.requires('Delay');
		this.defaultsheetx = this.sheetx;
		this.defaultsheety = this.sheety;
		this.bind('PathColorChange', function () {
			if (this.sheety < 7){
				this.sheety += 7;
			} else if (this.sheetx < 2) {
				this.sheetx += 2;	
			} else {
				this.sheetx = this.defaultsheetx;
				this.sheety = this.defaultsheety;
			}
			this.sprite(this.sheetx, this.sheety);
		}).bind('PathColorDefault', function () {
			this.sheetx = this.defaultsheetx;
			this.sheety = this.defaultsheety;
			this.sprite(this.sheetx, this.sheety);
		}).bind('DiscoParty', function () {
			var discoFunction = function () {
				Crafty.trigger('PathColorChange');
			}
			this.delay(discoFunction, 400, 22);
		});
	}
});


Crafty.c('PathU', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 3;
		this.requires('SolidTile, spr_u_path, Path');
	}
});

Crafty.c('Path3', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 3;
		this.requires('SolidTile, spr_3_path, Path');
	}
});

Crafty.c('PathC', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 3;
		this.requires('SolidTile, spr_3_path, Path');
		this._flipX = true;
	}
});

Crafty.c('PathFSlash', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 4;
		this.requires('SolidTile, spr_fslash_path, Path');
	}
});

Crafty.c('PathN', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 4;
		this.requires('SolidTile, spr_n_path, Path');
	}
});

Crafty.c('PathBSlash', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 5;
		this.requires('SolidTile, spr_bslash_path, Path');
	}
});

Crafty.c('Glass', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 7;
		this.requires('SolidTile, spr_glass');
	}
});

Crafty.c('ZebraFloor', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 7;
		this.requires('SolidTile, spr_zebra_floor');
	}
});

Crafty.c('Oven', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 6;
		this.requires('SolidTile, spr_oven');
	},
	
	interact: function (shift, ctrl) {
		Crafty.trigger("Eat");
	}
});

Crafty.c('PathDot', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 6;
		this.requires('SolidTile, spr_dot_path, Path');
	}
});

Crafty.c('PathCross', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 5;
		this.requires('SolidTile, spr_cross_path, Path');
	}
});

Crafty.c('Pool', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 9;
		this.requires('SolidTile, spr_pool');
	}
});

Crafty.c('TileShadow', {
	init: function () {
		this.requires('Tile, spr_tileShadow');	
	}
});

Crafty.c('Sink', {
	init: function () {
		this.sheetx = 2;
		this.sheety = 0;
		this.requires('SolidTile, spr_sink');	
	}
});

Crafty.c('WallHole', {
	init: function () {
		this.sheetx = 3;
		this.sheety = 0;
		this.requires('SolidTile, spr_wall_hole');	
	}
});

Crafty.c('Block', {
	init: function () {
		this.sheetx = 2;
		this.sheety = 1;
		this.requires('SolidTile, spr_block');	
	}
});

Crafty.c('Bed', {
	interact: function (shift, ctrl) {
		Crafty.trigger('Sleep');
	}
});

Crafty.c('BedBottom', {
	init: function () {
		this.sheetx = 3;
		this.sheety = 1;
		this.requires('SolidTile, spr_bed_bottom, Bed');
	}
});

Crafty.c('BedTop', {
	init: function () {
		this.sheetx = 2;
		this.sheety = 2;
		this.requires('SolidTile, spr_bed_top, Bed');
	}
});

Crafty.c('ClosetTop', {
	init: function () {
		this.sheetx = 1;
		this.sheety = 8;
		this.requires('SolidTile, spr_closet_top');
	}
});

Crafty.c('ClosetBottom', {
	init: function () {
		this.sheetx = 0;
		this.sheety = 8;
		this.requires('SolidTile, spr_closet_bottom');
	}
});

Crafty.c('Lever1', {
	init: function () {
		this.sheetx = 3;
		this.sheety = 8;
		this.requires('SolidTile, spr_lever');
	},
	
	interact: function (shift, ctrl) {
		if (shift){
			Crafty.trigger('XSpaceSpeedDown');
			this.sheetx = 2;
			this.sheety = 8;
			this.sprite(this.sheetx, this.sheety);
		} else if (ctrl) {
			Crafty.trigger('XSpaceSpeedDefault');
			this.sheetx = 3;
			this.sheety = 8;
			this.sprite(this.sheetx, this.sheety);
		} else {
			Crafty.trigger('XSpaceSpeedUp');
			this.sheety = 9;
			this.sheetx = 2;
			this.sprite(this.sheetx, this.sheety);
		}
	}
});

Crafty.c('Lever2', {
	init: function () {
		this.sheetx = 3;
		this.sheety = 8;
		this.requires('SolidTile, spr_lever');
	},
	
	interact: function (shift, ctrl) {
		if (shift){
			Crafty.trigger('YSpaceSpeedDown');
			this.sheetx = 2;
			this.sheety = 8;
			this.sprite(this.sheetx, this.sheety);
		} else if (ctrl) {
			Crafty.trigger('YSpaceSpeedDefault');
			this.sheetx = 3;
			this.sheety = 8;
			this.sprite(this.sheetx, this.sheety);
		} else {
			Crafty.trigger('YSpaceSpeedUp');
			this.sheety = 9;
			this.sheetx = 2;
			this.sprite(this.sheetx, this.sheety);
		}
	}
});

Crafty.c('ControlPanel', {
	init: function () {
		this.sheetx = 3;
		this.sheety = 9;
		this.requires('SolidTile, spr_colorful_buttons');
	},
	
	interact: function (shift, ctrl) {
		if (ctrl) {
			Crafty.trigger('PathColorDefault');	
		} else if (shift) {
			Crafty.trigger('DiscoParty');
		} else {
			Crafty.trigger('PathColorChange');
		} 
	}
});